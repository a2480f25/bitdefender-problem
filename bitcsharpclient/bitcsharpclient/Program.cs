﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace bitcsharpclient
{
    using ScannerMock = bitcsharpclient.BitDefScannerMock.UnsafeNativeMethods;
    class Program
    {
        static void CallBackFunction(string strParam, string strParam2)
        {
            Console.WriteLine(strParam + " " + strParam2);
        }

        static void ScannerResultFunction(int resultCode)
        {
            if (resultCode == 0)
            {
                Console.WriteLine("c# successfully stopped scanning. return code = 0\n");
                return;
            }
            if (resultCode == -1)
            {
                Console.WriteLine("c# forcefully stopped scanning. return code = -1\n");
                return;
            }
            Console.WriteLine(resultCode);
        }


        static void Main(string[] args)
        {
            BitDefScannerMock.CallBack callback_delegate = new BitDefScannerMock.CallBack(CallBackFunction);
            IntPtr intptr_delegate = Marshal.GetFunctionPointerForDelegate(callback_delegate);

            BitDefScannerMock.ScannerCallBack scannerResultDelegate = new BitDefScannerMock.ScannerCallBack(ScannerResultFunction);
            IntPtr intptr_scannerResultDelegate = Marshal.GetFunctionPointerForDelegate(scannerResultDelegate);

            IntPtr mockScannerSingleton = ScannerMock.getInstance();
            Console.WriteLine("c# attempting to start  scanner thread.");
            int status = ScannerMock.startScanner(mockScannerSingleton, intptr_delegate, intptr_scannerResultDelegate);
            if (status == 0)
            {
                Console.WriteLine("c# Got 0: scanning started - thread dispatched.\n");
            }
            Console.WriteLine("c# sleeping 2 sec.");
            System.Threading.Thread.Sleep(2000);
            Console.WriteLine("c# attempting to start SECOND scaner thread.");
            status = ScannerMock.startScanner(mockScannerSingleton, intptr_delegate, intptr_scannerResultDelegate);
            if (status == -2)
            {
                Console.WriteLine("c# Got -2: scanner already running, nothing to do.\n");
            }
            Console.WriteLine("c# sleeping 5 seconds then forcefully stopping scanning thread.");
            System.Threading.Thread.Sleep(5000);
            Console.WriteLine("c# attempting to stop scanner thread.");
            ScannerMock.stopScanner(mockScannerSingleton);
            Console.ReadKey();
            Environment.Exit(0);
            //ScannerMock.startScanner(mockScannerSingleton);
            //bool isScannerRunning = ScannerMock.isScannerRunning(mockScannerSingleton);
            //Console.WriteLine(isScannerRunning);
            //ScannerMock.stopScanner(mockScannerSingleton);
            //ScannerMock.disposeScannerMock(mockScannerSingleton);



            //ScannerMock.startScanner();
            //ScannerMock.stopScanner();
            //Boolean a = ScannerMock.isScannerRunning();
        }
}

    /// <summary>
    /// A C-Sharp interface to the DLL, written in C++.
    /// </summary>
    static class BitDefScannerMock
    {
        //private static BitDefScannerMock comm = new BitDefScannerMock();
        /// <summary>
        /// The native methods in the DLL's unmanaged code.
        /// </summary>
        internal static class UnsafeNativeMethods
        {
            const string _dllLocation = "C:\\Users\\Administrator\\Desktop\\bitdefender\\Debug\\bitdll.dll";
            [DllImport(_dllLocation, EntryPoint = "?startScanner@ScannerMock@ScannerAPI@@QAEHPAV12@P6GXPB_W1@ZP6GXH@Z@Z")]
            public static extern int startScanner(IntPtr pScannerMockObject, IntPtr pCallBack, IntPtr pScanningResultCallback);

            [DllImport(_dllLocation, EntryPoint = "?stopScanner@ScannerMock@ScannerAPI@@QAEHPAV12@@Z")]
            public static extern int stopScanner(IntPtr pScannerMockObject);

            [DllImport(_dllLocation, EntryPoint = "?isScannerRunning@ScannerMock@ScannerAPI@@QAE_NPAV12@@Z")]
            public static extern bool isScannerRunning(IntPtr pScannerMockObject);

            [DllImport(_dllLocation, EntryPoint = "?getInstance@ScannerMock@ScannerAPI@@SA?AV12@XZ")]
            public static extern IntPtr getInstance();

            [DllImport(_dllLocation, EntryPoint = "?disposeScannerMock@ScannerMock@ScannerAPI@@QAEXPAV12@@Z")]
            public static extern void disposeScannerMock(IntPtr pScannerMockObject);

            //[DllImport(_dllLocation, EntryPoint = "?disposeScannerMock@ScannerMock@ScanrAPI@@QAEXPAV12@@Z")]
            //public static delegate void FooCallbackType(int a, int b, int c);

            [DllImport(_dllLocation, EntryPoint = "?performActionWithCallBack@ScannerMock@ScannerAPI@@QAEXP6GXPB_W@Z@Z", CharSet =CharSet.Unicode)]
            public static extern void performActionWithCallBack(IntPtr pCallBack);
        }

        public delegate void CallBack([In][MarshalAs(UnmanagedType.LPWStr)] string strParam, [In][MarshalAs(UnmanagedType.LPWStr)] string strParam2);
        public delegate void ScannerCallBack([In][MarshalAs(UnmanagedType.I4)] int intErrorCode);

    }
}
