// bitdll.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "bitdll.h"
#include <thread>
#include <future>
#include <stdlib.h>

#include <random>

#include <iostream>
#include <ctime>
#include <atlbase.h>
#include <atlconv.h>
#include <sstream>
#include <atltime.h>
#include <thread>
#include <mutex>



//BITDLL_API void hello() {
//	MessageBox(NULL, (LPCWSTR)L"My Message", (LPCWSTR)L"My title", MB_ICONWARNING | MB_CANCELTRYCONTINUE);
//};



namespace ScannerAPI {
	
	using namespace std;

	std::vector<std::future<void>> pending_futures;
	bool shouldStopScanning = false;
	std::mutex shouldStopScanning_mutex;  // protects g_i

	static ScannerMock& get() {
		static ScannerMock instance;
		return instance;
	}

	 BITDLL_API ScannerMock ScannerMock::getInstance() {
		static ScannerMock instance;
		return instance;
	}

	 BITDLL_API void ScannerMock::disposeScannerMock(ScannerMock * a_pObject) {
		 if (a_pObject != NULL) {
			 delete a_pObject;
			 a_pObject = NULL;
		 }
	 }

	 BITDLL_API void setState(ScannerMock * a_pObject, ScannerState state) {
		 if (a_pObject != NULL) {
			 a_pObject->setState(state);
		 }
	}

    void ScannerMock::setState(ScannerState state) {
		scannerState = state;
	}

	ScannerState ScannerMock::getState() {
		return scannerState;
	}
	
	//-------------------
	LPCTSTR g_stringArray[] =
	{
		L"C:\\vir.exe",
		L"C:\\vir2.exe",
		L"C:\\vir3.dll",
		L"C:\\vir4.bat"
	};

	LPCTSTR g2_stringArray[] =
	{
		L"troj::dropper",
		L"malware_generic",
		L"patch::hacktool",
		L"patch::vir"
	};

	BITDLL_API bool ScannerMock::isScannerRunning(ScannerMock * a_pObject) {
		if (a_pObject != NULL) {
			return a_pObject->isScannerRunning();
		}
	}

	bool ScannerMock::isScannerRunning(void) {
		return scannerState == Started;
	}

	void ScannerMock::getTimeDateString(LPCTSTR* str) {
		CTime theTime;
		CString s;

		theTime = CTime::GetCurrentTime();
		s += theTime.Format(L"%A, %B %d, %Y at %H:%M:%S.");

		int sizeOfString = (s.GetLength() + 1);
		LPTSTR lpsz = new TCHAR[sizeOfString];
		_tcscpy_s(lpsz, sizeOfString, s);
		*str = lpsz;
	}

	void sendCurrentTimestamp(ScannerMock *mock, PCallBack pCallBack) {
		LPCTSTR ana;
		mock->getTimeDateString(&ana);
		pCallBack(ana, L"");
	}

	int randomInt(int start, int end) {
		return start + (rand() % (int)(end - start + 1));
	}

	int randomInt(double start, double end) {
		return (int)(start + (rand() % (int)(end - start + 1)));
	}

	void startScanner_thread(ScannerMock *mock, PCallBack pCallBack, PCallBackInt scanningResultCallback) {
		mock->setState(Started);
	
		//print out start date time
		sendCurrentTimestamp(mock, pCallBack);

		std::cout << "Starting Scanner..." << endl;
		std::cout << "Scanner Started." << endl;
		
		if (pCallBack) {
			size_t stSizeOfArray = sizeof(g_stringArray) / sizeof(LPCTSTR);

			int threadRunningTime = randomInt(10,30);
			int aux = 30 / stSizeOfArray;
			//random nr of threats 1..stSizeOfArray
			size_t aux2 = stSizeOfArray / randomInt(1, stSizeOfArray);
			int sleepFor = randomInt(1,5);

			for (size_t i = 0; i < aux2, sleepFor<threadRunningTime; i++) {
				if (shouldStopScanning) {
					sendCurrentTimestamp(mock, pCallBack);
					scanningResultCallback(-1);
					return;
				}
				pCallBack(g_stringArray[i], g2_stringArray[i]);
				aux = randomInt(1,sleepFor);
				sleepFor += aux;
				std::this_thread::sleep_for(std::chrono::seconds(aux));
			}
		}
		sendCurrentTimestamp(mock, pCallBack);
		scanningResultCallback(0);
	}

	int stopScanner_thread(ScannerMock *mock) {
		std::lock_guard<std::mutex> lock(shouldStopScanning_mutex);
		shouldStopScanning = true;

		//std::cout << std::this_thread::get_id() << ": " << g_i << '\n';
		std::cout << "Stopping Scanner..." << endl;
		std::cout << "Informed scanner thread to stop.." << endl;
		mock->setState(Stopped);
		
		return 0;
	}

	BITDLL_API int ScannerMock::startScanner(ScannerMock * a_pObject, PCallBack pCallBack, PCallBackInt scanningResultCallback) {
		if (a_pObject != NULL) {
			if (a_pObject->getState() == Started) {
				return -2;
			} else if (a_pObject->getState() == Stopped) {
				a_pObject->startScanner(pCallBack, scanningResultCallback);
				return 0;
			}
		}
	}

	void ScannerMock::startScanner(PCallBack pCallBack, PCallBackInt scanningResultCallback) {
		std::future<void> fut;
		fut = std::async(&startScanner_thread, this, pCallBack, scanningResultCallback);
		pending_futures.push_back(std::move(fut));
	}

	BITDLL_API int ScannerMock::stopScanner(ScannerMock * a_pObject) {
		if (a_pObject != NULL) {
			if (a_pObject->getState() == Started) {
				return a_pObject->stopScanner();
			}
		}
	}

	int ScannerMock::stopScanner() {
		std::future<int> fut = std::async(&stopScanner_thread, this);

		try {
			return fut.get();
		}
		catch (std::exception& e) {
			std::cout << e.what() << std::endl;
			return -1;
		}
		return 0;
	}
}