/* File : bitdll.i */
/* swig -c++ -ruby bitdll.i */
%module bitdllswig

%{
#include "bitdll.h"
%}

/* Let's just grab the original header file here */
%include <windows.i>
%include "bitdll.h"

__declspec(dllexport) ULONG __stdcall foo(DWORD, __int32);