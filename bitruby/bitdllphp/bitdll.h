#pragma once
//#include "stdafx.h"
#include<iostream>
#include"windows.h"
#ifdef BITDLL_EXPORTS
#define BITDLL_API __declspec(dllexport)
#else
#define BITDLL_API __declspec(dllimport)
#endif

//BITDLL_API void hello();
typedef void(__stdcall *PCallBack)(LPCTSTR s, LPCTSTR p);
typedef void(__stdcall *PCallBackInt)(int a);
namespace  ScannerAPI {
	enum ScannerState { Started, Stopped };	

	class  ScannerMock {
	public:
		BITDLL_API static ScannerMock getInstance();
		BITDLL_API void disposeScannerMock(ScannerMock* a_pObject);
		
		BITDLL_API int startScanner(ScannerMock * a_pObject, PCallBack pCallBack, PCallBackInt scanningResultCallback);
		void startScanner(PCallBack pCallBack, PCallBackInt scanningResultCallback);
		BITDLL_API int stopScanner(ScannerMock* a_pObject);
		int stopScanner();
		BITDLL_API bool isScannerRunning(ScannerMock* a_pObject);
		bool isScannerRunning(void);

		void getTimeDateString(LPCTSTR * str);

		BITDLL_API void setState(ScannerState state);
		ScannerState getState();

	private:
		
	     ScannerState scannerState = Stopped;
	};
}