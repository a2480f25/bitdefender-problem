# mylibrary.rb

module MyLibrary
  extend FFI::Library
  ffi_lib "bitdll"
  #class PCallBack < FFI::Struct
  #    layout :s8, :char, :f32, :float, :s32, :int
  #end
  callback :cbOne, [:pointer, :pointer], :void
  callback :cbTwo, [:int], :void
  attach_function :getInstance, '?getInstance@ScannerMock@ScannerAPI@@SA?AV12@XZ', [], :pointer
  attach_function :startScanner, '?startScanner@ScannerMock@ScannerAPI@@QAEHPAV12@P6GXPB_W1@ZP6GXH@Z@Z', [:pointer, :cbOne, :cbTwo], :int # note empty array for functions taking zero arguments
  attach_function :stopScanner, '?stopScanner@ScannerMock@ScannerAPI@@QAEHPAV12@@Z', [:pointer], :int
  
end