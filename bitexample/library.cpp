#include "library.h"
#include "ScannerMock.h"

#include <iostream>
#include "stdafx.h"

using namespace std;
void hello() {
    //std::cout << "Hello, World!" << std::endl;

    ScannerMock scannerMock = ScannerMock();
    scannerMock.startScanner();
    std::cout<<"Is scanner running: " << scannerMock.isScannerRunning()<<endl;
    scannerMock.stopScanner();
    std::cout<<"Is scanner running: " << scannerMock.isScannerRunning()<<endl;
}

