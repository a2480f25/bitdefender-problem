//
// Created by Good on 03/04/2017.
//

#ifndef BITEXAMPLE_SCANNERMOCK_H
#define BITEXAMPLE_SCANNERMOCK_H

enum ScannerState { Started, Stopped };
class ScannerMock {

public:
    int startScanner();
    int stopScanner();
    bool isScannerRunning();

    void setState(ScannerState state);

private:

    ScannerState scannerState;

};


#endif //BITEXAMPLE_SCANNERMOCK_H
