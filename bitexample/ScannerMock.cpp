//
// Created by Good on 03/04/2017.
//

#include "ScannerMock.h"
#include <thread>
#include <future>
#include <iostream>
#include "stdafx.h"

using namespace std;

void ScannerMock::setState(ScannerState state) {
    scannerState = state;
}

bool ScannerMock::isScannerRunning() {
    return scannerState == Started;
}

int startScanner_thread(ScannerMock *mock) {
    std::cout<<"Starting Scanner..."<<endl;
    std::cout<<"Scanner Started."<<endl;
    mock->setState(Started);
    return 0;
}

int stopScanner_thread(ScannerMock *mock) {
    std::cout<<"Stopping Scanner..."<<endl;
    std::cout<<"Scanner Stopped."<<endl;
    mock->setState(Stopped);

    return 0;
}

int ScannerMock::startScanner() {
    std::future<int> fut;
    fut = std::async(&startScanner_thread, this);

    try {
        return fut.get();
    } catch( std::exception& e ) {
        std::cout << e.what() << std::endl;
        return -1;
    }
}

int ScannerMock::stopScanner() {
    std::future<int> fut =  std::async(&stopScanner_thread, this);

    try {
        return fut.get();
    } catch( std::exception& e ) {
        std::cout << e.what() << std::endl;
        return -1;
    }
}