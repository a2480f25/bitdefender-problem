// bitdefclr.cpp : main project file.

#include "stdafx.h"
#include "bitdll.h"
#include <iostream>
#include <string>

using namespace System;
using namespace System::Runtime::InteropServices;
using namespace System::Threading::Tasks;

//#pragma unmanaged  
//
//typedef int(__stdcall *ANSWERCB)(int, int);
//
//int TakesCallback(ANSWERCB fp, int n, int m) {
//	printf_s("[unmanaged] got callback address, calling it...\n");
//	return fp(n, m);
//}
//#pragma managed 

public delegate void CallBackFunctionDelegate(LPCTSTR, LPCTSTR);
public delegate void ScannerResultFunction(int);

std::string TstrToStdStr(LPCTSTR psz)
{
	std::string s;
	if (psz == NULL) return s;
	if (sizeof(TCHAR) == sizeof(wchar_t))
	{
		std::wstring ws = psz;
		int wlen = (int)ws.length();
		// get needed space
		int slen = (int)wcstombs(NULL, &ws[0], 2 * wlen);

		if (slen > 0)
		{
			s.resize(slen, '\0');
			wcstombs(&s[0], &ws[0], slen);
		}
	}
	return s;
}

void PrintStats(LPCTSTR n, LPCTSTR m) {
	std::cout << TstrToStdStr(n) << " " << TstrToStdStr(m) <<std::endl;
}

void PrintScannerExitCodes(int resultCode) {
	if (resultCode == 0)
	{
		Console::WriteLine("c++ cli successfully stopped scanning. return code = 0\n");
		return;
	}
	if (resultCode == -1)
	{
		Console::WriteLine("c++ cli forcefully stopped scanning. return code = -1\n");
		return;
	}
	Console::WriteLine(resultCode);
}

//[DllImport("bitdll.dll", CharSet = CharSet::Ansi)]
//extern "C" static ScannerMock getInstance();

/*value struct TraditionalDLL
{
	[DllImport("TraditionalDLL2.dll")]
	static public void
		TakesAString([MarshalAs(UnmanagedType::LPStr)]String^);

	const string _dllLocation = "C:\\Users\\Administrator\\Desktop\\bitdefender\\Debug\\bitdll.dll";
	[DllImport(_dllLocation, EntryPoint = "?startScanner@ScannerMock@ScannerAPI@@QAEHPAV12@P6GXPB_W1@ZP6GXH@Z@Z")]
	public static extern int startScanner(IntPtr pScannerMockObject, IntPtr pCallBack, IntPtr pScanningResultCallback);
};*/

void callme() {
	CallBackFunctionDelegate^ fp = gcnew CallBackFunctionDelegate(PrintStats);
	GCHandle gch = GCHandle::Alloc(fp);
	IntPtr ip = Marshal::GetFunctionPointerForDelegate(fp);
	PCallBack cb = static_cast<PCallBack>(ip.ToPointer());

	ScannerResultFunction^ fp2 = gcnew ScannerResultFunction(PrintScannerExitCodes);
	GCHandle gch2 = GCHandle::Alloc(fp2);
	IntPtr ip2 = Marshal::GetFunctionPointerForDelegate(fp2);
	PCallBackInt cb2 = static_cast<PCallBackInt>(ip2.ToPointer());
	Console::WriteLine("[managed] sending delegate as callback...");

	ScannerAPI::ScannerMock a = ScannerAPI::ScannerMock();
	a.getInstance();

	// force garbage collection cycle to prove  
	// that the delegate doesn't get disposed  
	GC::Collect();

	int status = a.startScanner(&a, cb, cb2);
	if (status == 0)
	{
		Console::WriteLine("c++ cli Got 0: scanning started - thread dispatched.\n");
	}
	Console::WriteLine("c++ cli sleeping 2 sec.");
	System::Threading::Thread::Sleep(2000);
	Console::WriteLine("c++ cli attempting to start SECOND scaner thread.");
	status = a.startScanner(&a, cb, cb2);
	if (status == -2)
	{
		Console::WriteLine("c++ cli Got -2: scanner already running, nothing to do.\n");
	}
	Console::WriteLine("c++ cli sleeping 5 seconds then forcefully stopping scanning thread.");
	System::Threading::Thread::Sleep(5000);
	Console::WriteLine("c++ cli attempting to stop scanner thread.");
	a.stopScanner(&a);
	
	// release reference to delegate  
	gch.Free();
	Console::ReadKey();
}
int main() {
	callme();
}